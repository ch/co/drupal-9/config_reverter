<?php

namespace Drupal\config_reverter;

use Drupal\config_update\ConfigDiffer;
use Drupal\config_update\ConfigListerWithProviders;
// Alias the class from config_update to avoid conflict.
use Drupal\config_update\ConfigReverter as ConfigUpdater;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * A set of functions to allow a module to revert its configuration.
 */
class ConfigReverter {

  /**
   * Constructor.
   */
  public function __construct(
        private readonly EntityTypeManagerInterface $entityTypeManager,
        private readonly ConfigListerWithProviders $configLister,
        private readonly ConfigDiffer $configDiffer,
        private readonly ConfigUpdater $configUpdater,
        private readonly LoggerInterface $logger,
  ) {
  }

  /**
   * Lists differing config items.
   *
   * Taken from config_update_ui/src/Commands/ConfigUpdateUiCommands.php.
   *
   * Lists config items that differ from the versions provided by your
   * installed modules, themes, or install profile.
   *
   * @param string $type
   *   Run the report for: module, theme, profile, or "type" for config entity
   *   type.
   * @param string $name
   *   The machine name of the module, theme, etc. to report on. See
   *   config-list-types to list types for config entities; you can also use
   *   system.all for all types, or system.simple for simple config.
   *
   * @return array
   *   An array of differing configuration items.
   */
  protected function getDifferentItems(string $type, string $name): array {
    [$activeList, $installList, $optionalList] = $this->configLister->listConfig($type, $name);
    $addedItems = array_diff($activeList, $installList, $optionalList);
    $activeAndAddedItems = array_diff($activeList, $addedItems);
    $differentItems = [];
    foreach ($activeAndAddedItems as $name) {
      $active = $this->configUpdater->getFromActive('', $name);
      $extension = $this->configUpdater->getFromExtension('', $name);
      if (!$this->configDiffer->same($active, $extension)) {
        $differentItems[] = $name;
      }
    }
    sort($differentItems);

    return $differentItems;
  }

  /**
   * {@inheritdoc}
   */
  public function revertMultiple(string $type, string $name) {
    $different = $this->getDifferentItems($type, $name);
    foreach ($different as $name) {
      $this->revert($name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function revert(string $name) {
    $type = $this->configLister->getTypeNameByConfigName($name);
    // The lister gives NULL if simple configuration, but the reverter expects
    // 'system.simple' so we convert it.
    if ($type === NULL) {
      $type = 'system.simple';
    }
    $shortname = $this->getConfigShortname($type, $name);
    if ($this->configUpdater->revert($type, $shortname)) {
      $this->logger->info(dt('The configuration item @name was reverted to its source.', ['@name' => $name]));
    }
    else {
      $this->logger->error(dt('There was an error and the configuration item @name was not reverted.', ['@name' => $name]));
    }
  }

  /**
   * Gets the config item shortname given the type and name.
   *
   * Taken from config_update_ui/src/Commands/ConfigUpdateUiCommands.php.
   *
   * @param string $type
   *   The type of the config item.
   * @param string $name
   *   The name of the config item.
   *
   * @return string
   *   The shortname for the configuration item.
   */
  protected function getConfigShortname(string $type, string $name): string {
    $shortname = $name;
    if ($type != 'system.simple') {
      $definition = $this->entityTypeManager->getDefinition($type);
      $prefix = $definition->getConfigPrefix() . '.';
      if (str_starts_with($name, $prefix)) {
        $shortname = substr($name, strlen($prefix));
      }
    }

    return $shortname;
  }

}
