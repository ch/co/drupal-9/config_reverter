<?php

namespace Drupal\config_reverter;

/**
 * Interface for config_reverter.config_reverter service.
 */
interface ConfigReverterInterface {

  /**
   * Reverts multiple config items to extension provided version.
   *
   * Taken from config_update_ui/src/Commands/ConfigUpdateUiCommands.php.
   *
   * Reverts a set of config items to the versions provided by installed
   * modules, themes, or install profiles. A set is all differing items from
   * one extension, or one type of configuration.
   *
   * @param string $type
   *   Type of set to revert: "module" for all items from a module, "theme" for
   *   all items from a theme, "profile" for all items from the install profile,
   *   or "type" for all items of one config entity type. See
   *   config-different-report to list config items that are different.
   * @param string $name
   *   The machine name of the module, theme, etc. to revert items of. All
   *   items in the corresponding config-different-report will be reverted.
   */
  public function revertMultiple(string $type, string $name);

  /**
   * Reverts a config item.
   *
   * Taken from config_update_ui/src/Commands/ConfigUpdateUiCommands.php.
   *
   * Reverts one config item in active storage to the version provided by an
   * installed module, theme, or install profile.
   *
   * @param string $name
   *   The config item to revert. See config-different-report to list config
   *   items that are different.
   */
  public function revert(string $name);

}
