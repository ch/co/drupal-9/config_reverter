config_reverter
===============

This module implements a subset of the functionality provided by the
config_updater_ui module.  That module provides user-facing functionality (via
drush/web interface) to revert/update config values that are provided by a
module and which then change in a later module version.

However, it's also useful for modules to be able to manage config updates
themselves. Suppose example_module wishes to implement `hook_update_N()` and
have it revert the config values it provides in e.g. config/install,
config/optional. Then it might implement:

```
example_module_update_8001() { 
  $configReverter = \Drupal::service('config_reverter.config_reverter');
  $configReverter->revertMultiple('module', 'example_module'); 
}
```

Note that this will revert all settings defined by example_module so should be
used with care!

Or, to revert specific pieces of config,

```
example_module_update_8001() {
  $configReverter = \Drupal::service('config_reverter.config_reverter');
  $configReverter->revert('views.view.example_view_name');
  $configReverter->revert('field.field.node.job.body');
}
```

If you want to more thoroughly inspect the differences between live vs. desired
config, enable the config_update_ui module and run commands like

```
drush config:different-report module example_module
```

See also https://www.drupal.org/project/drupal/issues/2684081 ; at some point
this kind of functionality might make it into Drupal core.
